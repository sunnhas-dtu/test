//
//  ContentView.swift
//  test
//
//  Created by Magnus Hansen on 07/12/2019.
//  Copyright © 2019 Magnus Hansen. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
